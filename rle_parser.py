def parse_rle(file_name):

    with open(file_name) as input_file:
        rle = input_from_file(input_file.read())

    x, y = 0, 0
    alive = set()

    digits = ''

    for character in rle:

        if character.isdigit():
            digits += character

        elif character == 'b':
            x += int(digits) if digits else 1
            digits = ''

        elif character == 'o':
            for _ in range(int(digits) if digits else 1):
                alive.add((x, y))
                x += 1
            digits = ''

        elif character == '$':
            y += int(digits) if digits else 1
            x = 0
            digits = ''

    return alive


def input_from_file(rle):
    return ''.join([line for line in rle.split('\n')
        if '$' in line or '!' in line and '#' not in line and line != ''])
