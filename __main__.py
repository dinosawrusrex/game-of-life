import argparse
import os
import random

import game_of_life
import rle_parser


SEEDS = {
    'blinker': {(0,0), (0,1), (0,2)},
    'toad': {(0,1), (0,2), (0,3), (1,2), (1,3), (1,4)},
    'beacon': {(0,1), (0,2), (1,1), (1,2), (2,3), (2,4), (3,3), (3,4)},
    'glider': {(1,-1), (1,0), (1,1), (0,1), (-1,0)},
    'by flops': {(1,3), (2,1), (2,3), (3,5), (4,0), (4,1), (4,2),
        (4,3), (4,4), (5,5), (6,1), (6,3), (7,3)},
    'spaceship': {(-1,0), (1,0), (2,1), (2,2), (2,3), (2,4), (1,4), (0,4),
        (-1,3)},
}


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    group = parser.add_mutually_exclusive_group()
    group.add_argument('-s', '--seed', action='store', choices=SEEDS.keys())
    rle_arg = group.add_argument('-r', '--rle', action='store')

    parser.add_argument('-t', '--turns', action='store', type=int, default=100)

    args = parser.parse_args()

    if args.rle is not None:
        if args.rle not in os.listdir('./'):
            raise argparse.ArgumentError(
                rle_arg, f'{args.rle} not found in current directory.'
            )
        living = rle_parser.parse_rle(args.rle)

    elif args.seed is None:
        living = random.choice(list(SEEDS.values()))
    else: # choices already limited since it was set in add_argument
        living = SEEDS[args.seed]

    game_of_life.run_game(living, args.turns)


