import collections
import os
import time


BOUNDARIES = collections.namedtuple( 'BOUNDARIES', 'min_x, min_y, max_x, max_y')


def run_game(living, turn_limit):
    boundaries = BOUNDARIES(0, 0, 0, 0)

    for turn in range(turn_limit+1):
        os.system('cls' if os.name == 'nt' else 'clear')
        print('Turn: {}\n'.format(turn))
        boundaries = update_boundaries(boundaries, living)
        render(living, boundaries)
        living = get_next_living(living)
        time.sleep(0.2)


def render(living, boundaries):
    for y in range(boundaries.min_y, boundaries.max_y+1):
        for x in range(boundaries.min_x, boundaries.max_x+1):
            if (x, y) in living:
                print('#', end=' ')
            else:
                print(' ', end=' ')
        print('')
    print('')


def update_boundaries(boundaries, living):
    return BOUNDARIES(
        min([boundaries.min_x] + [coordinates[0] for coordinates in living]),
        min([boundaries.min_y] + [coordinates[1] for coordinates in living]),
        max([boundaries.max_x] + [coordinates[0] for coordinates in living]),
        max([boundaries.max_y] + [coordinates[1] for coordinates in living])
    )


def get_neighbours(coordinates):
    x, y = coordinates
    return set(
        (i, j) for i in range(x-1, x+2)
        for j in range(y-1, y+2) if (i, j) != coordinates
    )


def get_next_living(living):
    next = set()

    for coordinates in living:
        neighbour_coordinates = get_neighbours(coordinates)
        if len(neighbour_coordinates.intersection(living)) in [2, 3]:
            next.add(coordinates)

        for dead in filter(lambda c: c not in living, neighbour_coordinates):
            if len(get_neighbours(dead).intersection(living)) == 3:
                next.add(dead)

    return next
